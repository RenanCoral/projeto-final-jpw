import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';


/*import header from './pages/'*/
//Rotas
import campeao from './pages/campeao/campeao';
import mapa from './pages/mapa/mapa';
import item from './pages/item/item';
import monstro from './pages/monstro/monstro';
import modoJogo from './pages/modoJogo/modoJogo';

function Router() {
  return (
    <BrowserRouter>
      {/* <Header></Header>*/}
      <Switch>
        <Route path="/campeao" exact component={campeao} />
        <Route path="/mapa" exact component={mapa} />
        <Route path="/item" exact component={item} />
        <Route path="/monstro" exact component={monstro} />
        <Route path="/modoJogo" exact component={modoJogo} />
      </Switch>
    </BrowserRouter>
  );
}

export default Router;