import React from "react";
import "../style.css";
import axios from "axios";

export default class GamesForm extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      modojogos: []
    }
  }


  componentDidMount = () => {
    this.handleGet()
  }


  handleGet = () => {
    axios.get("http://localhost:8080/modojogo").then(res => {
      this.setState({
        modojogos: res.data
      })
    })
  }
  handlePost = (modojogo) => {
    axios.post("http://localhost:8080/modojogo", modojogo);
  }

  handleSubmit = (event) => {
    event.preventDefault()
  }

  handlePost() {
    const game = {
      nome: this.state.modojogos.nome,
      descricao: this.state.modojogos.descricao
    }
  }

  handleChange = (event) => {
    event.preventDefault()

    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleDelete = (index) => {
    axios.delete("http://localhost:8080/modojogo/" + index);
    this.handleGet();
  }

  render() {

    return <div className="container">
      <form className="center" onSubmit={this.handleSubmit}>
        <label type="text" >Nome</label>
        <input type="text" id="nome" placeholder="Nome" value={this.state.modojogos.nome} onChange={this.handleChange}></input>
        <label type="text" >Descrição</label>
        <input type="text" id="descricao" placeholder="descrição" value={this.state.modojogos.descricao} onChange={this.handleChange}></input>
        <button className="btn btn-primary" type="submit">Cadastrar</button>
      </form>

      <h1>Lista de Modos de Jogo</h1>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Descrição</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {
            this.state.modojogos.map((modojogo) => {
              return (
                <tr key={modojogo._id}>
                  <td>{modojogo.nome}</td>
                  <td>{modojogo.descricao}</td>
                  <td><button className="btn btn-primary" onClick={() => this.handleDelete(modojogo._id)}>Deletar</button></td>
                </tr>
              )
            })
          }

        </tbody>
      </table>
    </div>
  }
}