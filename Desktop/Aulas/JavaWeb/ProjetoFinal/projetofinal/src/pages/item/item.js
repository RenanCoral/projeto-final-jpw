import React from "react";
import "../style.css";
import axios from "axios";

export default class GamesForm extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      items: []
    }
  }


  componentDidMount = () => {
    this.handleGet()
  }


  handleGet = () => {
    axios.get("http://localhost:8080/item").then(res => {
      this.setState({
        items: res.data
      })
    })
  }
  handlePost = (item) => {
    const url = 'http://localhost:8080/item'
    var request = axios.post(url, item)

    request.then((response) => {
      console.log(response.data)
    })

  }

  handleSubmit = (item) => {
    this.handlePost(item)
  }

  handleChange = (event) => {
    event.preventDefault()

    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleDelete = (index) => {
    axios.delete("http://localhost:8080/item/" + index);
    this.handleGet();
  }

  render() {

    return <div className="container">
      <form className="center" onSubmit={this.handleSubmit}>
        <label type="text" >Nome</label>
        <input type="text" id="nome" placeholder="Nome" value={this.state.items.nome} onChange={this.handleChange}></input>
        <label type="text" >Valor</label>
        <input type="text" id="valor" placeholder="Valor" value={this.state.items.valor} onChange={this.handleChange}></input>
        <label type="text" >Descrição</label>
        <input type="text" id="descricao" placeholder="Descrição" value={this.state.items.descricao} onChange={this.handleChange}></input>
        <button className="btn btn-primary" type="submit">Cadastrar</button>
      </form>

      <h1>Lista de Itens</h1>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">valor</th>
            <th scope="col">Descrição</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {
            this.state.items.map((item) => {
              return (
                <tr key={item._id}>
                  <td>{item.nome}</td>
                  <td>{item.valor}</td>
                  <td>{item.descricao}</td>
                  <td><button className="btn btn-primary" onClick={() => this.handleDelete(item._id)}>Deletar</button></td>
                </tr>
              )
            })
          }

        </tbody>
      </table>
    </div>
  }
}