import React from "react";
import "../style.css";
import axios from "axios";

export default class GamesForm extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      monstros: []
    }
  }


  componentDidMount = () => {
    this.handleGet()
  }


  handleGet = () => {
    axios.get("http://localhost:8080/monstro").then(res => {
      this.setState({
        monstros: res.data
      })
    })
  }
  handlePost = (monstro) => {
    axios.post("http://localhost:8080/monstro", monstro);
  }

  handleSubmit = (event) => {
    event.preventDefault()
  }

  handlePost() {
    const game = {
      nome: this.state.monstros.nome,
      tipo: this.state.monstros.tipo
    }
  }

  handleChange = (event) => {
    event.preventDefault()

    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleDelete = (index) => {
    axios.delete("http://localhost:8080/monstro/" + index);
    this.handleGet();
  }

  render() {

    return <div className="container">
      <form className="center" onSubmit={this.handleSubmit}>
        <label type="text" >Nome</label>
        <input type="text" id="nome" placeholder="Nome" value={this.state.monstros.nome} onChange={this.handleChange}></input>
        <label type="text" >Descrição</label>
        <input type="text" id="tipo" placeholder="Descrição" value={this.state.monstros.tipo} onChange={this.handleChange}></input>
        <button className="btn btn-primary" type="submit">Cadastrar</button>
      </form>

      <h1>Lista de Monstros</h1>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Descrição</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {
            this.state.monstros.map((monstro) => {
              return (
                <tr key={monstro._id}>
                  <td>{monstro.nome}</td>
                  <td>{monstro.descricao}</td>
                  <td><button className="btn btn-primary" onClick={() => this.handleDelete(monstro._id)}>Deletar</button></td>
                </tr>
              )
            })
          }

        </tbody>
      </table>
    </div>
  }
}