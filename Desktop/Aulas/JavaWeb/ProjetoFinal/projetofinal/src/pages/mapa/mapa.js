import React from "react";
import "../style.css";
import axios from "axios";

export default class GamesForm extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      mapas: []
    }
  }

  componentDidMount = () => {
    this.handleGet()
  }


  handleGet = () => {
    axios.get("http://localhost:8080/mapa").then(res => {
      this.setState({
        mapas: res.data
      })
    })
  }
  handlePost = (mapa) => {
    console.log(mapa)
    axios.post("http://localhost:8080/mapa", mapa);
  }

  handleSubmit = () => {
    var a = this.state.mapas.nome;
    var b = this.state.mapas.quantidadeLanes;
    const mapa = {
      nome: a,
      quantidadeLanes: b
    }
    this.handlePost(mapa)
  }

  handleChange = (event) => {
    event.preventDefault()

    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleDelete = (index) => {
    axios.delete("http://localhost:8080/mapa/" + index);
    this.handleGet();
  }

  render() {

    return <div className="container">
      <form className="center" onSubmit={this.handleSubmit}>
        <label type="text" >Nome</label>
        <input type="text" id="nome" placeholder="Nome" value={this.state.mapas.nome} onChange={this.handleChange}></input>
        <label type="text" >Quant Lanes</label>
        <input type="text" id="lanes" placeholder="Tipo" value={this.state.mapas.lanes} onChange={this.handleChange}></input>
        <button className="btn btn-primary" type="submit">Cadastrar</button>
      </form>

      <h1>Lista de Mapas</h1>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Quantidade Lanes</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {
            this.state.mapas.map((mapa) => {
              return (
                <tr key={mapa.nome}>
                  <td>{mapa.nome}</td>
                  <td>{mapa.quantidadeLanes}</td>
                  <td><button className="btn btn-primary" onClick={() => this.handleDelete(mapa._id)}>Deletar</button></td>
                </tr>
              )
            })
          }

        </tbody>
      </table>

    </div>
  }
}