import React from "react";
import "../style.css";
import axios from "axios";

export default class GamesForm extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      campeaos: []
    }
  }


  componentDidMount = () => {
    this.handleGet()
  }


  handleGet = () => {
    axios.get("http://localhost:8080/campeao").then(res => {
      this.setState({
        campeaos: res.data
      })
    })
  }
  handlePost = (campeao) => {
    axios.post("http://localhost:8080/campeao", campeao);
  }

  handleSubmit = (event) => {
    event.preventDefault()
  }

  handlePost() {
    const game = {
      nome: this.state.chanpion.nome,
      tipo: this.state.chanpion.tipo
    }
  }

  handleChange = (event) => {
    event.preventDefault()

    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleDelete = (index) => {
    axios.delete("http://localhost:8080/campeao/" + index);
    this.handleGet();
  }

  render() {

    return <div className="container">
      <form className="center" onSubmit={this.handleSubmit}>
        <label type="text" >Nome</label>
        <input type="text" id="nome" placeholder="Nome" value={this.state.campeaos.nome} onChange={this.handleChange}></input>
        <label type="text" >Tipo</label>
        <input type="text" id="tipo" placeholder="Tipo" value={this.state.campeaos.tipo} onChange={this.handleChange}></input>
        <button className="btn btn-primary" type="submit">Cadastrar</button>
      </form>

      <h1>Lista de campeões</h1>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Nome</th>
            <th scope="col">Tipo</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {
            this.state.campeaos.map((campeao) => {
              return (
                <tr key={campeao._id}>
                  <td>{campeao.nome}</td>
                  <td>{campeao.tipo}</td>
                  <td><button className="btn btn-primary" onClick={() => this.handleDelete(campeao._id)}>Deletar</button></td>
                </tr>
              )
            })
          }

        </tbody>
      </table>

    </div>
  }
}