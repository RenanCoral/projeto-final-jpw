import React from 'react';
import Router from './router';
import { Button } from 'reactstrap';

function App() {
  return (
    <div className="App">
      <Router />
    </div>
  );
}

export default App;
